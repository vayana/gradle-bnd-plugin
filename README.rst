Using gradle-bnd-plugin
=======================

Build
-----

To build the plugin, just go to the plugin root directory and enter the command :

  $ gradle jar

This should place the gradle-bnd-plugin-<version>.jar under build/libs.

Copy the same into an appropriate directory where you will install your gradle plugins (eg. ~/gradle-plugins)


Use
---

To use this plugin, make the following changes to your build.gradle.

a. Import
+++++++++

Add the following buildscript section and import statement

.. code-block:: groovy
  buildscript {
      repositories {
          mavenCentral()
      }
      dependencies {
          classpath fileTree(dir: "${System.properties['user.home']}/gradle-plugins", include: '*.jar')
          classpath 'biz.aQute:bndlib:1.50.0'
      }
  }

  import in.vayana.gradle_plugins.GradleBndPlugin

b. Use
++++++

Apply the plugin

.. code-block:: groovy

  apply plugin: GradleBndPlugin


c. Configure
++++++++++++

You will need to configure the behaviour of the plugin as follows

.. code-block:: groovy

  bnd {
    condense = true // omit this or set to false if no uber jar is required to be built
    exclusions = ['groovy-all'] as Set // list of jar files (without version number or 
                                       // extensions to not be included in the uberjar 
                                       // in case the above condense parameter is set 
                                       // to true (note: groovy-all is just a sample data)
    // note : the following are just sample data - customise as needed
    instructions = [
        'Import-Package': 'org.osgi.framework;version="[1.4,2)",*',
        'Export-Package': 'my.package.export,*',
        'Bundle-Activator':'my.package.BundleActivator',
        'Bundle-Description':'My Bundle Description',
        'Bundle-Vendor':'Vendor Name',
        'Bundle-Version':'1.0.0'
    ]
  }


Profit
++++++

Have fun. Raise requests in the project issues or send me pull requests if you have some cool functionality to add.


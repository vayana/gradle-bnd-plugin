/*******************************************************************************
*   Copyright 2011 Vayana Services Private Limited                             *
*                                                                              *
*   Licensed under the Apache License, Version 2.0 (the "License");            *
*   you may not use this file except in compliance with the License.           *
*   You may obtain a copy of the License at                                    *
*                                                                              *
*       http://www.apache.org/licenses/LICENSE-2.0                             *
*                                                                              *
*   Unless required by applicable law or agreed to in writing, software        *
*   distributed under the License is distributed on an "AS IS" BASIS,          *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
*   See the License for the specific language governing permissions and        *
*   limitations under the License.                                             *
*******************************************************************************/

package in.vayana.gradle_plugins

import java.util.jar.JarFile
import java.util.jar.JarEntry
import java.util.jar.JarOutputStream
import aQute.lib.osgi.Analyzer
import org.gradle.api.*

import org.slf4j.Logger
import org.slf4j.LoggerFactory


class GradleBndPluginExtension {
   def Map<String,String> instructions = new HashMap<String,String>() 
   def String exclusions = ''
   def Boolean condense = false
   def Boolean includeTestDependencies = false
}

class GradleBndPlugin implements Plugin<Project> {
    def void apply(Project project) {
        Logger log = LoggerFactory.getLogger('gradle-bnd-plugin')
        project.extensions.bnd = new GradleBndPluginExtension()

        project.configurations { exclusions }
        project.tasks.jar {
            doLast() {
                def oldJar = project.jar.archivePath
                def excludedJars = []
                def includedJars = []
                def pattern = /(.*)-([0-9][a-zA-Z0-9_\.^-]*)(-SNAPSHOT)?(.standalone)?.jar$/
                def classFilePattern =  /(.*)\/[^\/]*.class/
                if(project.bnd.condense) {
                    // move old jar away
                    def newJarName = oldJar.toString().replaceFirst('.jar$',".standalone.jar")
                    log.debug("Moving " + oldJar.toString() + " to " + newJarName)
                    oldJar.renameTo(new File(newJarName))

                    // create the new jar with same name as the output of the jar task

                    def outputJar = new JarOutputStream(
                                    new FileOutputStream(oldJar.toString()))
                    def depJars = (project.configurations.compile as List) + new File(newJarName) 
                    if (project.bnd.includeTestDependencies)
                        depJars = depJars + (project.configurations.testCompile 
as List)
                    def excluded = (project.configurations.exclusions as List).collect {it.name} 
                    depJars.each { file -> 
                        if (!(file.name in excluded)) {
                            log.debug(file.name + " included")
                            includedJars.add(file.name)
                            def matcher = file.name =~ pattern
                            if (! matcher.matches() ) {
                                println("***** Unable to get version number for " + file.name)
                            }
                            def jarVersion = matcher[0][2]
                            jarVersion = (jarVersion =~ /([a-zA-Z-_]+)/).replaceAll("")
                            jarVersion = (jarVersion =~ /[\.-]*$/).replaceAll("")
                            def packageInfoSet = [] as Set
                            // extract each entry from the jar file
                            def jarFile = new JarFile(file.absolutePath)
                            jarFile.entries().each { entry ->
                                // copy each entry over into the destination jar
                                def outEntry = new JarEntry(entry.getName())
                                outEntry.setTime(entry.getTime())
                                try {
                                    outputJar.putNextEntry(outEntry)
                                    if (! entry.isDirectory()) {
                                        outputJar.write(
                                            jarFile.getInputStream(entry).getBytes())
                                        def cfMatcher = entry.getName() =~ classFilePattern
                                        if (cfMatcher.matches()) {
                                            def dir = cfMatcher[0][1]
                                            if (!packageInfoSet.contains(dir)) {
                                                packageInfoSet.add(dir)
                                                def packageEntry = new JarEntry(dir + "/packageinfo")
                                                outputJar.putNextEntry(packageEntry)
                                                outputJar.write(("version " + jarVersion).getBytes("utf-8"))
                                            }
                                        }
                                    }
                                } catch (java.util.zip.ZipException ze) {
                                    // this happens quite often due to duplicates
                                    // especially META-INF/MANIFEST.MF
                                    // or recreation of the same directories eg. org., com. etc
                                    ; // ignore
                                }
                            }
                        } else {
                            log.debug(file.name + " excluded")
                            excludedJars.add(file.toString())
                        }
                    }
                    outputJar.close()
                }

                Analyzer analyzer = new Analyzer()
                analyzer.setJar(new File(oldJar.toString()))
                project.bnd.instructions.each {name,value ->
                    analyzer.setProperty(name,value)
                }
                analyzer.setProperty("Included-Jars",includedJars.join(':'))
                analyzer.setProperty("-nodefaultversion","true")
                
                excludedJars.each{jarPath -> analyzer.addClasspath(new File(jarPath))}
                java.util.jar.Manifest manifest = analyzer.calcManifest()
                def jarFile = new File(oldJar.toString())
                def tmpFileName = oldJar.toString() + ".tmp"
                log.debug("Renaming (Copying due to windows jdk bug)" + oldJar.toString() + " to " + tmpFileName)
                new File(tmpFileName) << jarFile.bytes
        
                def outputJar = new JarOutputStream(
                                new FileOutputStream(oldJar.toString()))
                log.debug("Creating a new jar file " + oldJar.toString())
                def jarFileHandle = new File(tmpFileName)
                jarFile = new JarFile(jarFileHandle) // , false, java.util.zip.ZipFile.OPEN_DELETE)
                jarFile.entries().each { entry ->
                    if (entry.toString() != "META-INF/MANIFEST.MF") {
                        def outEntry = new JarEntry(entry.getName())
                        outEntry.setTime(entry.getTime())
                        try {
                            outputJar.putNextEntry(outEntry)
                            if (! entry.isDirectory()) {
                                outputJar.write(
                                    jarFile.getInputStream(entry).getBytes())
                            }
                        } catch (java.util.zip.ZipException ze) {
                            ; // ignore
                        }
                    }
                }
                
                jarFile.close();
                log.debug("Deleting file " + tmpFileName)
                new File(tmpFileName).delete();
                def jarEntry = new JarEntry("META-INF/MANIFEST.MF")
                outputJar.putNextEntry(jarEntry)
                manifest.write(outputJar)
                outputJar.close()
            }

            project.tasks.install.repositories.mavenInstaller.pom.whenConfigured {pom ->
                def exclusions = project.configurations.exclusions.dependencies.collect { dep ->
                                    [dep.group, dep.name, dep.version] }
                def condensed = pom.dependencies.findAll { dep -> !exclusions.contains([dep.groupId,dep.artifactId,dep.version]) }
                def constrs = condensed.collect {
                    def vals = [it.groupId,it.artifactId,it.version]
                    if(it.type != null) { vals.add(it.type) }
                    vals.join(":")
                }.join(",")
                pom.dependencies.removeAll { dep -> !exclusions.contains([dep.groupId,dep.artifactId,dep.version]) }
                pom.project {
                    groupId = project.groupId
                    artifactId = project.artifactId
                    version = project.version
                    properties {
                        in_vayana_condensed_dependencies = constrs
                    }
                }
            }

        }
    }
}

